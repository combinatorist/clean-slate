from selenium import webdriver
from webdriver_manager.chrome import ChromeDriverManager
from dotenv import load_dotenv
from os import getenv
from selenium.webdriver.chrome.options import Options
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait

options = Options()
if getenv("CLEAN_SLATE_DONT_WAIT_FOR_CAPTCHA"):
    options.headless = True

load_dotenv()


site = 'https://cjs.shelbycountytn.gov/CJS/Account/Login'
driver = webdriver.Chrome(ChromeDriverManager().install(), options=options)

name_or_case_number = 'smith, john'
date_of_birth = "11/07/1981"

if getenv("CLEAN_SLATE_USERNAME"):
    username = getenv("CLEAN_SLATE_USERNAME")
else:
    username = input('Please enter your user name to log in: ')
if getenv("CLEAN_SLATE_PASSWORD"):
    password = getenv("CLEAN_SLATE_PASSWORD")
else:
    password = input('As well as your password: ')
##name_or_case_number = input("Kindly enter a name in 'last, first' form OR enter a case number please.")
##date_of_birth = input("Once more, please enter a DOB in MM/DD/YYYY form. Thank you!!")


def login_portal(username, password):
    
    driver.get(site)
    
    user_name_entry = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.ID, 'UserName')))

    ## User Info entry and click login
    user_name_entry.send_keys(username)
    password_entry = driver.find_element_by_id('Password')
    password_entry.send_keys(password)
    login_click = driver.find_element_by_css_selector('.btn')
    login_click.click()
    
    #Make sure we've logged in, then navigate to search page
    # WebDriverWait(driver, 30).until(
    # EC.presence_of_element_located((By.CLASS_NAME, 'btn-description')))
    search_site = 'https://cjs.shelbycountytn.gov/CJS/Home/Dashboard/29'
    driver.get(search_site)



## Here for multiple case numbers
def case_numbers():
    
    case_links = driver.find_elements_by_class_name('caseLink')

    if len(case_links) == 0:
        print('No cases matching this DOB')

    for i in range(len(case_links)):
        case_links[i].click()
        
    ##Here is where the downloading starts
        scrape_case_number = WebDriverWait(driver, 30).until(
        EC.presence_of_element_located((By.XPATH, "//div[@id='divCaseInformation_body']//span[contains(text(), 'Case Number')]/parent::*")))
                                        
        print(scrape_case_number.text)
        disp_number = driver.find_elements_by_id('DispositionEventsPrintSection')
        for i in range(len(disp_number)):
            print(disp_number[i].text)
            
    ##This goes back to search results
        back_to_search = driver.find_element_by_xpath("//p[contains(text( ), 'Search Results')]")
        back_to_search.click()

## Enter search of site
def search_and_expunge():

    record_search = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.ID, 'caseCriteria_SearchCriteria')))
    
    record_search.send_keys(name_or_case_number)
    click_submit = driver.find_element_by_id("btnSSSubmit")
    click_submit.click()


    ## Entering in DOB
    dob_drop_down = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, "//th[@data-field='DateOfBirthSort']//span[@title='Sort / Filter Options']")))

    dob_drop_down.click()
    
    dob_text_wait = WebDriverWait(driver, 30).until(
    EC.presence_of_element_located((By.XPATH, "//div[@class='k-animation-container']//input[@class='k-input']")))
    dob_text_wait.click()
    
    dob_text_wait.send_keys(date_of_birth)
    dob_submit = driver.find_element_by_xpath("//div[@class='k-animation-container']//button[@type='submit']")
    dob_submit.click()


    ##From name search results (after DOB is entered), gets to info to be downloaded
    ##From here I can find all the caseLinks and store them in a list, and then loop thru each.
    ##Each going click case# (@detailed), scrape info, press back, find next case#, repeat
    case_numbers()



login_portal(username, password)


try:
    driver.find_element_by_class_name('g-recaptcha')
    title_text = driver.find_element_by_xpath('/html/head/title').get_attribute('textContent')
    print(f"captcha detected on page: {title_text}")
    if getenv("CLEAN_SLATE_DONT_WAIT_FOR_CAPTCHA"):
        print("captcha detected in environment where not exptected; terminating script")
        quit()
except NoSuchElementException:
    print('no captcha')

if not getenv("CLEAN_SLATE_DONT_WAIT_FOR_CAPTCHA"):
    input("Hit enter when you've completed the captcha.")
else:
    print("If you get a Captcha, configure your run differently to wait for it.")

search_and_expunge()





##RN I can get relevant text by search by id the 'disp_number = driver.find_elements_by_id('DispositionEventsPrintSection')
##I'm sure I can get even more refined by only looking for the text that's within that block, under a couple diff headers
##Tjat should give me enough to only take what I'd like. FULL CODE:

##for i in range(len(disp_number)):
##	print(disp_number[i].text)


## PLaces for waits are before login, before dob search,

